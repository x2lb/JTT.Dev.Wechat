﻿using JTT.Dev.Toolkit.Time;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace JTT.Dev.Wechat
{
    /// <summary>
    /// 微信各种实体
    /// </summary>
    namespace Entity
    {

        #region 微信接收消息实体

        /// <summary>
        /// 微信接收消息实体
        /// </summary>
        [XmlRoot("xml")]
        public class JTTWechatReceiveMessage
        {
            #region "开发者微信号"

            /// <summary>
            /// 开发者微信号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ToUserName { get; set; }

            /// <summary>
            /// 开发者微信号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("ToUserName")]
            public XmlNode[] CDataToUserName
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ToUserName) };
                }
                set
                {
                    if (value == null)
                    {
                        ToUserName = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ToUserName = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ToUserName = null;
                        return;
                    }
                    ToUserName = Content.Value;
                }
            }

            #endregion

            #region "发送方帐号"

            /// <summary>
            /// 发送方帐号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String FromUserName { get; set; }

            /// <summary>
            /// 发送方帐号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("FromUserName")]
            public XmlNode[] CDataFromUserName
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(FromUserName) };
                }
                set
                {
                    if (value == null)
                    {
                        FromUserName = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        FromUserName = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        FromUserName = null;
                        return;
                    }
                    FromUserName = Content.Value;
                }
            }

            #endregion

            #region "消息创建时间"

            /// <summary>
            /// 消息创建时间
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("CreateTime")]
            public long? CreateTime { get; set; }

            #endregion

            #region "消息类型"

            /// <summary>
            /// 消息类型
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String MsgType { get; set; }

            /// <summary>
            /// 消息类型的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("MsgType")]
            public XmlNode[] CDataMsgType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(MsgType) };
                }
                set
                {
                    if (value == null)
                    {
                        MsgType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        MsgType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        MsgType = null;
                        return;
                    }
                    MsgType = Content.Value;
                }
            }

            #endregion

            #region "消息编号"

            /// <summary>
            ///  消息编号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("MsgId")]
            public long? MsgId { get; set; }

            #endregion

            #region "文本消息内容"

            /// <summary>
            /// 文本消息内容
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Content { get; set; }

            /// <summary>
            /// 文本消息内容的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Content")]
            public XmlNode[] CDataContent
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Content) };
                }
                set
                {
                    if (value == null)
                    {
                        Content = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Content = null;
                        return;
                    }
                    dynamic TempContent = value[0];
                    if (TempContent == null)
                    {
                        Content = null;
                        return;
                    }
                    Content = TempContent.Value;
                }
            }

            #endregion

            #region "图片链接"

            /// <summary>
            /// 图片链接
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String PicUrl { get; set; }

            /// <summary>
            /// 图片链接的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("PicUrl")]
            public XmlNode[] CDataPicUrl
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(PicUrl) };
                }
                set
                {
                    if (value == null)
                    {
                        PicUrl = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        PicUrl = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        PicUrl = null;
                        return;
                    }
                    PicUrl = Content.Value;
                }
            }

            #endregion

            #region "媒体编号"

            /// <summary>
            /// 媒体编号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String MediaId { get; set; }

            /// <summary>
            /// 媒体编号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("MediaId")]
            public XmlNode[] CDataMediaId
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(MediaId) };
                }
                set
                {
                    if (value == null)
                    {
                        MediaId = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        MediaId = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        MediaId = null;
                        return;
                    }
                    MediaId = Content.Value;
                }
            }

            #endregion

            #region "语音格式"

            /// <summary>
            /// 语音格式
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Format { get; set; }

            /// <summary>
            /// 语音格式的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Format")]
            public XmlNode[] CDataFormat
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Format) };
                }
                set
                {
                    if (value == null)
                    {
                        Format = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Format = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Format = null;
                        return;
                    }
                    Format = Content.Value;
                }
            }

            #endregion

            #region "视频消息缩略图的媒体编号"

            /// <summary>
            /// 视频消息缩略图的媒体编号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ThumbMediaId { get; set; }

            /// <summary>
            /// 视频消息缩略图的媒体编号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("ThumbMediaId")]
            public XmlNode[] CDataThumbMediaId
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ThumbMediaId) };
                }
                set
                {
                    if (value == null)
                    {
                        ThumbMediaId = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ThumbMediaId = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ThumbMediaId = null;
                        return;
                    }
                    ThumbMediaId = Content.Value;
                }
            }

            #endregion

            #region "地理位置纬度"

            /// <summary>
            /// 地理位置纬度
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Location_X")]
            public Double? Location_X { get; set; }

            #endregion

            #region "地理位置经度"

            /// <summary>
            /// 地理位置经度
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Location_Y")]
            public Double? Location_Y { get; set; }

            #endregion

            #region "地图缩放大小"

            /// <summary>
            /// 地图缩放大小
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Scale")]
            public Double? Scale { get; set; }

            #endregion

            #region "地理位置信息"

            /// <summary>
            /// 地理位置信息
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Label { get; set; }

            /// <summary>
            /// 地理位置信息的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Label")]
            public XmlNode[] CDataLabel
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Label) };
                }
                set
                {
                    if (value == null)
                    {
                        Label = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Label = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Label = null;
                        return;
                    }
                    Label = Content.Value;
                }
            }

            #endregion

            #region "消息标题"

            /// <summary>
            /// 消息标题
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Title { get; set; }

            /// <summary>
            /// 消息标题的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Title")]
            public XmlNode[] CDataTitle
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Title) };
                }
                set
                {
                    if (value == null)
                    {
                        Title = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Title = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Title = null;
                        return;
                    }
                    Title = Content.Value;
                }
            }

            #endregion

            #region "消息描述"

            /// <summary>
            /// 消息描述
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Description { get; set; }

            /// <summary>
            /// 消息描述的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Description")]
            public XmlNode[] CDataDescription
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Description) };
                }
                set
                {
                    if (value == null)
                    {
                        Description = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Description = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Description = null;
                        return;
                    }
                    Description = Content.Value;
                }
            }

            #endregion

            #region "消息链接"

            /// <summary>
            /// 消息链接
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Url { get; set; }

            /// <summary>
            /// 消息链接的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Url")]
            public XmlNode[] CDataUrl
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Url) };
                }
                set
                {
                    if (value == null)
                    {
                        Url = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Url = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Url = null;
                        return;
                    }
                    Url = Content.Value;
                }
            }

            #endregion

            #region "事件类型"

            /// <summary>
            /// 事件类型
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Event { get; set; }

            /// <summary>
            /// 事件类型的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Event")]
            public XmlNode[] CDataEvent
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Event) };
                }
                set
                {
                    if (value == null)
                    {
                        Event = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Event = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Event = null;
                        return;
                    }
                    Event = Content.Value;
                }
            }

            #endregion

            #region "事件KEY值"

            /// <summary>
            /// 事件KEY值
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String EventKey { get; set; }

            /// <summary>
            /// 事件KEY值的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("EventKey")]
            public XmlNode[] CDataEventKey
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(EventKey) };
                }
                set
                {
                    if (value == null)
                    {
                        EventKey = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        EventKey = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        EventKey = null;
                        return;
                    }
                    EventKey = Content.Value;
                }
            }

            #endregion

            #region "二维码的ticket"

            /// <summary>
            /// 二维码的ticket
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Ticket { get; set; }

            /// <summary>
            /// 二维码的ticket的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Ticket")]
            public XmlNode[] CDataTicket
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Ticket) };
                }
                set
                {
                    if (value == null)
                    {
                        Ticket = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Ticket = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Ticket = null;
                        return;
                    }
                    Ticket = Content.Value;
                }
            }

            #endregion

            #region "地理位置纬度"

            /// <summary>
            /// 地理位置纬度
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Latitude")]
            public Double? Latitude { get; set; }

            #endregion

            #region "地理位置经度"

            /// <summary>
            /// 地理位置经度
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Longitude")]
            public Double? Longitude { get; set; }

            #endregion

            #region "地理位置精度"

            /// <summary>
            /// 地理位置精度
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Precision")]
            public Double? Precision { get; set; }

            #endregion

            #region "模板消息状态"

            /// <summary>
            /// 模板消息状态
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Status { get; set; }

            /// <summary>
            /// 模板消息状态的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Status")]
            public XmlNode[] CDataStatus
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Status) };
                }
                set
                {
                    if (value == null)
                    {
                        Status = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Status = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Status = null;
                        return;
                    }
                    Status = Content.Value;
                }
            }

            #endregion

            #region 多客服

            #region 多客服账号名称

            /// <summary>
            /// 客服账号名称
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String KFAccount { get; set; }

            /// <summary>
            /// 客服账号名称的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("KfAccount")]
            public XmlNode[] CDataKFAccount
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(KFAccount) };
                }
                set
                {
                    if (value == null)
                    {
                        KFAccount = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        KFAccount = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        KFAccount = null;
                        return;
                    }
                    KFAccount = Content.Value;
                }
            }

            #endregion

            #region 多客服转接来源账号名称

            /// <summary>
            /// 多客服转接来源账号名称
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String FromKFAccount { get; set; }

            /// <summary>
            /// 多客服转接来源账号名称的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("FromKfAccount")]
            public XmlNode[] CDataFromKFAccount
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(FromKFAccount) };
                }
                set
                {
                    if (value == null)
                    {
                        FromKFAccount = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        FromKFAccount = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        FromKFAccount = null;
                        return;
                    }
                    FromKFAccount = Content.Value;
                }
            }

            #endregion

            #region 多客服转接目标账号名称

            /// <summary>
            /// 多客服转接目标账号名称
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ToKFAccount { get; set; }

            /// <summary>
            /// 多客服转接目标账号名称的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("ToKfAccount")]
            public XmlNode[] CDataToKFAccount
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ToKFAccount) };
                }
                set
                {
                    if (value == null)
                    {
                        ToKFAccount = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ToKFAccount = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ToKFAccount = null;
                        return;
                    }
                    ToKFAccount = Content.Value;
                }
            }

            #endregion

            #endregion
        }

        #endregion

        #region 微信返回文本消息实体

        /// <summary>
        /// 微信返回文本消息实体
        /// </summary>
        [XmlRoot("xml")]
        public class JTTWechatTextReturnMessage
        {
            #region "构造函数"

            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatTextReturnMessage()
            {
                MsgType = "text";
                DateTime NowDateTime = DateTime.Now;
                CreateTime = NowDateTime.ConvertToTimeStamp();
            }

            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatTextReturnMessage(JTTWechatReceiveMessage _ReceiveMessage)
            {
                MsgType = "text";
                DateTime NowDateTime = DateTime.Now;
                CreateTime = NowDateTime.ConvertToTimeStamp();
                ToUserName = _ReceiveMessage.FromUserName;
                FromUserName = _ReceiveMessage.ToUserName;
            }

            #endregion

            #region "接收方帐号"

            /// <summary>
            /// 接收方帐号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ToUserName { get; set; }

            /// <summary>
            /// 接收方帐号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("ToUserName")]
            public XmlNode[] CDataToUserName
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ToUserName) };
                }
                set
                {
                    if (value == null)
                    {
                        ToUserName = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ToUserName = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ToUserName = null;
                        return;
                    }
                    ToUserName = Content.Value;
                }
            }

            #endregion

            #region "开发者微信号"

            /// <summary>
            /// 开发者微信号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String FromUserName { get; set; }

            /// <summary>
            /// 开发者微信号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("FromUserName")]
            public XmlNode[] CDataFromUserName
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(FromUserName) };
                }
                set
                {
                    if (value == null)
                    {
                        FromUserName = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        FromUserName = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        FromUserName = null;
                        return;
                    }
                    FromUserName = Content.Value;
                }
            }

            #endregion

            #region "消息创建时间"

            /// <summary>
            /// 消息创建时间
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("CreateTime")]
            public long? CreateTime { get; set; }

            #endregion

            #region "消息类型"

            /// <summary>
            /// 消息类型
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String MsgType { get; set; }

            /// <summary>
            /// 消息类型的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("MsgType")]
            public XmlNode[] CDataMsgType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(MsgType) };
                }
                set
                {
                    if (value == null)
                    {
                        MsgType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        MsgType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        MsgType = null;
                        return;
                    }
                    MsgType = Content.Value;
                }
            }

            #endregion

            #region "回复的消息内容"

            /// <summary>
            /// 回复的消息内容
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Content { get; set; }

            /// <summary>
            /// 回复的消息内容的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("Content")]
            public XmlNode[] CDataContent
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Content) };
                }
                set
                {
                    if (value == null)
                    {
                        Content = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Content = null;
                        return;
                    }
                    dynamic TempContent = value[0];
                    if (TempContent == null)
                    {
                        Content = null;
                        return;
                    }
                    Content = TempContent.Value;
                }
            }

            #endregion
        }

        #endregion

        #region 微信返回图片消息实体

        /// <summary>
        /// 微信返回图片消息实体
        /// </summary>
        [XmlRoot("xml")]
        public class JTTWechatImageReturnMessage
        {
            #region "构造函数"

            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatImageReturnMessage()
            {
                MsgType = "image";
                DateTime NowDateTime = DateTime.Now;
                CreateTime = NowDateTime.ConvertToTimeStamp();
            }

            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatImageReturnMessage(JTTWechatReceiveMessage _ReceiveMessage)
            {
                MsgType = "image";
                DateTime NowDateTime = DateTime.Now;
                CreateTime = NowDateTime.ConvertToTimeStamp();
                ToUserName = _ReceiveMessage.FromUserName;
                FromUserName = _ReceiveMessage.ToUserName;
            }

            #endregion

            #region "接收方帐号"

            /// <summary>
            /// 接收方帐号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ToUserName { get; set; }

            /// <summary>
            /// 接收方帐号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("ToUserName")]
            public XmlNode[] CDataToUserName
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ToUserName) };
                }
                set
                {
                    if (value == null)
                    {
                        ToUserName = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ToUserName = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ToUserName = null;
                        return;
                    }
                    ToUserName = Content.Value;
                }
            }

            #endregion

            #region "开发者微信号"

            /// <summary>
            /// 开发者微信号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String FromUserName { get; set; }

            /// <summary>
            /// 开发者微信号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("FromUserName")]
            public XmlNode[] CDataFromUserName
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(FromUserName) };
                }
                set
                {
                    if (value == null)
                    {
                        FromUserName = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        FromUserName = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        FromUserName = null;
                        return;
                    }
                    FromUserName = Content.Value;
                }
            }

            #endregion

            #region "消息创建时间"

            /// <summary>
            /// 消息创建时间
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("CreateTime")]
            public long? CreateTime { get; set; }

            #endregion

            #region "消息类型"

            /// <summary>
            /// 消息类型
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String MsgType { get; set; }

            /// <summary>
            /// 消息类型的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("MsgType")]
            public XmlNode[] CDataMsgType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(MsgType) };
                }
                set
                {
                    if (value == null)
                    {
                        MsgType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        MsgType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        MsgType = null;
                        return;
                    }
                    MsgType = Content.Value;
                }
            }

            #endregion

            #region "媒体文件编号"

            /// <summary>
            /// 媒体文件编号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String MediaId { get; set; }

            /// <summary>
            /// 媒体文件编号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("MediaId")]
            public XmlNode[] CDataMediaId
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(MediaId) };
                }
                set
                {
                    if (value == null)
                    {
                        MediaId = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        MediaId = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        MediaId = null;
                        return;
                    }
                    MediaId = Content.Value;
                }
            }

            #endregion
        }

        #endregion

        #region 微信返回转入多客服消息实体

        /// <summary>
        /// 微信返回转入多客服消息实体
        /// </summary>
        [XmlRoot("xml")]
        public class JTTWechatCustomerServiceReturnMessage
        {
            #region "构造函数"

            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatCustomerServiceReturnMessage()
            {
                MsgType = "transfer_customer_service";
                DateTime NowDateTime = DateTime.Now;
                CreateTime = NowDateTime.ConvertToTimeStamp();
            }

            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatCustomerServiceReturnMessage(JTTWechatReceiveMessage _ReceiveMessage)
            {
                MsgType = "transfer_customer_service";
                DateTime NowDateTime = DateTime.Now;
                CreateTime = NowDateTime.ConvertToTimeStamp();
                ToUserName = _ReceiveMessage.FromUserName;
                FromUserName = _ReceiveMessage.ToUserName;
            }

            #endregion

            #region "接收方帐号"

            /// <summary>
            /// 接收方帐号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ToUserName { get; set; }

            /// <summary>
            /// 接收方帐号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("ToUserName")]
            public XmlNode[] CDataToUserName
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ToUserName) };
                }
                set
                {
                    if (value == null)
                    {
                        ToUserName = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ToUserName = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ToUserName = null;
                        return;
                    }
                    ToUserName = Content.Value;
                }
            }

            #endregion

            #region "开发者微信号"

            /// <summary>
            /// 开发者微信号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String FromUserName { get; set; }

            /// <summary>
            /// 开发者微信号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("FromUserName")]
            public XmlNode[] CDataFromUserName
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(FromUserName) };
                }
                set
                {
                    if (value == null)
                    {
                        FromUserName = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        FromUserName = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        FromUserName = null;
                        return;
                    }
                    FromUserName = Content.Value;
                }
            }

            #endregion

            #region "消息创建时间"

            /// <summary>
            /// 消息创建时间
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("CreateTime")]
            public long? CreateTime { get; set; }

            #endregion

            #region "消息类型"

            /// <summary>
            /// 消息类型
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String MsgType { get; set; }

            /// <summary>
            /// 消息类型的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("MsgType")]
            public XmlNode[] CDataMsgType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(MsgType) };
                }
                set
                {
                    if (value == null)
                    {
                        MsgType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        MsgType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        MsgType = null;
                        return;
                    }
                    MsgType = Content.Value;
                }
            }

            #endregion
        }

        #endregion

        #region 获取AccessToken结果实体

        [DataContract()]
        public class JTTWechatGetAccessToken
        {
            /// <summary>
            /// 授权码
            /// </summary>
            [DataMember(Name = "access_token")]
            public String AccessToken { get; set; }

            /// <summary>
            /// 失效时间（秒）
            /// </summary>
            [DataMember(Name = "expires_in")]
            public Int32 ExpireTime { get; set; }
        }

        #endregion

        #region 用户信息实体

        /// <summary>
        /// 用户信息实体
        /// </summary>
        [DataContract()]
        public class JTTWechatUserInfo
        {
            /// <summary>
            /// 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息
            /// </summary>
            [DataMember(Name = "subscribe")]
            public Int32 Subscribe { get; set; }

            /// <summary>
            ///  用户的标识，对当前公众号唯一
            /// </summary>
            [DataMember(Name = "openid")]
            public String OpenID { get; set; }

            /// <summary>
            /// 昵称
            /// </summary>
            [DataMember(Name = "nickname")]
            public String NickName { get; set; }

            /// <summary>
            /// 性别
            /// </summary>
            [DataMember(Name = "sex")]
            public Int32 Sex { get; set; }

            /// <summary>
            /// 城市
            /// </summary>
            [DataMember(Name = "city")]
            public String City { get; set; }

            /// <summary>
            /// 国家
            /// </summary>
            [DataMember(Name = "country")]
            public String Country { get; set; }

            /// <summary>
            /// 省份
            /// </summary>
            [DataMember(Name = "province")]
            public String Province { get; set; }

            /// <summary>
            /// 语言
            /// </summary>
            [DataMember(Name = "language")]
            public String Language { get; set; }

            /// <summary>
            /// 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
            /// </summary>
            [DataMember(Name = "headimgurl")]
            public String HeadImageURL { get; set; }

            /// <summary>
            /// 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
            /// </summary>
            [DataMember(Name = "subscribe_time")]
            public Int64 SubscribeTime { get; set; }

            /// <summary>
            /// 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
            /// </summary>
            [DataMember(Name = "unionid")]
            public String UnionID { get; set; }
        }

        #endregion

        #region 模板消息

        /// <summary>
        /// 模板消息实体
        /// </summary>
        [JsonObject()]
        [DataContract()]
        public class JTTWechatTemplateMessage
        {
            /// <summary>
            /// OpenID
            /// </summary>
            [JsonProperty(PropertyName = "touser")]
            [DataMember(Name = "touser")]
            public String ToUser { get; set; }

            /// <summary>
            /// 模板编号
            /// </summary>
            [JsonProperty(PropertyName = "template_id")]
            [DataMember(Name = "template_id")]
            public String TemplateID { get; set; }

            /// <summary>
            /// 点击后链接
            /// </summary>
            [JsonProperty(PropertyName = "url")]
            [DataMember(Name = "url")]
            public String URL { get; set; }

            /// <summary>
            /// 顶部颜色
            /// </summary>
            [JsonProperty(PropertyName = "topcolor")]
            [DataMember(Name = "topcolor")]
            public String TopColor { get; set; }

            /// <summary>
            /// 参数数据
            /// </summary>
            [JsonProperty(PropertyName = "data")]
            [DataMember(Name = "data")]
            public Dictionary<String, JTTWechatTemplateMessageParameterData> Data { get; set; }

        }

        /// <summary>
        /// 模板消息参数数据
        /// </summary>
        [JsonObject()]
        [DataContract()]
        public class JTTWechatTemplateMessageParameterData
        {
            /// <summary>
            /// 数值
            /// </summary>
            [JsonProperty(PropertyName = "value")]
            [DataMember(Name = "value")]
            public String Value { get; set; }

            /// <summary>
            /// 参数
            /// </summary>
            [JsonProperty(PropertyName = "color")]
            [DataMember(Name = "color")]
            public String Color { get; set; }
        }

        /// <summary>
        /// 模板消息发送返回
        /// </summary>
        [DataContract()]
        public class JTTWechatTemplateSendResult
        {
            /// <summary>
            /// 错误代码
            /// </summary>
            [DataMember(Name = "errcode")]
            public Int32 ErrorCode { get; set; }

            /// <summary>
            /// 错误信息
            /// </summary>
            [DataMember(Name = "errmsg")]
            public String ErrorMessage { get; set; }

            /// <summary>
            /// 信息编号
            /// </summary>
            [DataMember(Name = "msgid")]
            public Int64 MessageID { get; set; }
        }

        #endregion

        #region 网页授权

        /// <summary>
        /// 网页授权AccessToken信息
        /// </summary>
        [DataContract()]
        public class JTTWechatOAuthAccessToken
        {
            /// <summary>
            /// 网页授权接口调用凭证
            /// </summary>
            [DataMember(Name = "access_token")]
            public String AccessToken { get; set; }

            /// <summary>
            /// access_token接口调用凭证超时时间
            /// </summary>
            [DataMember(Name = "expires_in")]
            public Int32 ExpireInTime { get; set; }

            /// <summary>
            /// 用户刷新access_token
            /// </summary>
            [DataMember(Name = "refresh_token")]
            public String RefreshToken { get; set; }

            /// <summary>
            /// OpenID
            /// </summary>
            [DataMember(Name = "openid")]
            public String OpenID { get; set; }

            /// <summary>
            /// 作用域
            /// </summary>
            [DataMember(Name = "scope")]
            public String Scope { get; set; }
        }

        #endregion

        #region JSSDK

        #region "JSTicket信息"

        /// <summary>
        /// JSTicket信息
        /// </summary>
        [DataContract()]
        public class JTTWechatJSTicket
        {
            /// <summary>
            /// 错误代码
            /// </summary>
            [DataMember(Name = "errcode")]
            public Int32 ErrorCode { get; set; }

            /// <summary>
            /// 错误信息
            /// </summary>
            [DataMember(Name = "errmsg")]
            public String ErrorMessage { get; set; }

            /// <summary>
            /// 密钥
            /// </summary>
            [DataMember(Name = "ticket")]
            public String Ticket { get; set; }

            /// <summary>
            /// 过期时间
            /// </summary>
            [DataMember(Name = "expires_in")]
            public Int32 ExpireTime { get; set; }
        }

        #endregion

        #endregion

        #region 微信支付

        /// <summary>
        /// 微信支付异步通知信息
        /// </summary>
        [XmlRoot("xml")]
        [DataContract()]
        public class JTTWechatWXPayNotifyInfo
        {

            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatWXPayNotifyInfo() { }

            #region "签名"

            /// <summary>
            /// 签名
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Sign { get; set; }

            /// <summary>
            /// 签名的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("sign")]
            public XmlNode[] CDataSign
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Sign) };
                }
                set
                {
                    if (value == null)
                    {
                        Sign = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Sign = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Sign = null;
                        return;
                    }
                    Sign = Content.Value;
                }
            }

            #endregion

            #region "返回状态码"

            /// <summary>
            /// 返回状态码
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ReturnCode { get; set; }

            /// <summary>
            /// 返回状态码的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("return_code")]
            public XmlNode[] CDataReturnCode
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ReturnCode) };
                }
                set
                {
                    if (value == null)
                    {
                        ReturnCode = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ReturnCode = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ReturnCode = null;
                        return;
                    }
                    ReturnCode = Content.Value;
                }
            }

            #endregion

            #region "返回信息"

            /// <summary>
            /// 返回信息
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ReturnMessage { get; set; }

            /// <summary>
            /// 返回信息的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("return_msg")]
            public XmlNode[] CDataReturnMessage
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ReturnMessage) };
                }
                set
                {
                    if (value == null)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    ReturnMessage = Content.Value;
                }
            }

            #endregion

            #region "公众账号ID"

            /// <summary>
            /// 公众账号ID
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String AppID { get; set; }

            /// <summary>
            /// 公众账号ID的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("appid")]
            public XmlNode[] CDataAppID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(AppID) };
                }
                set
                {
                    if (value == null)
                    {
                        AppID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        AppID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        AppID = null;
                        return;
                    }
                    AppID = Content.Value;
                }
            }

            #endregion

            #region "商户号"

            /// <summary>
            /// 商户号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String MerchantID { get; set; }

            /// <summary>
            /// 商户号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("mch_id")]
            public XmlNode[] CDataMerchantID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(MerchantID) };
                }
                set
                {
                    if (value == null)
                    {
                        MerchantID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        MerchantID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        MerchantID = null;
                        return;
                    }
                    MerchantID = Content.Value;
                }
            }

            #endregion

            #region "设备号"

            /// <summary>
            /// 设备号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String DeviceInfo { get; set; }

            /// <summary>
            ///设备号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("device_info")]
            public XmlNode[] CDataDeviceInfo
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(DeviceInfo) };
                }
                set
                {
                    if (value == null)
                    {
                        DeviceInfo = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        DeviceInfo = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        DeviceInfo = null;
                        return;
                    }
                    DeviceInfo = Content.Value;
                }
            }

            #endregion

            #region "随机字符串"

            /// <summary>
            /// 随机字符串
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String NonceString { get; set; }

            /// <summary>
            ///随机字符串的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("nonce_str")]
            public XmlNode[] CDataNonceString
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(NonceString) };
                }
                set
                {
                    if (value == null)
                    {
                        NonceString = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        NonceString = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        NonceString = null;
                        return;
                    }
                    NonceString = Content.Value;
                }
            }

            #endregion

            #region "业务结果"

            /// <summary>
            /// 业务结果
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String RetultCode { get; set; }

            /// <summary>
            ///业务结果的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("result_code")]
            public XmlNode[] CDataRetultCode
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(RetultCode) };
                }
                set
                {
                    if (value == null)
                    {
                        RetultCode = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        RetultCode = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        RetultCode = null;
                        return;
                    }
                    RetultCode = Content.Value;
                }
            }

            #endregion

            #region "错误代码"

            /// <summary>
            /// 错误代码
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ErrorCode { get; set; }

            /// <summary>
            ///错误代码的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("err_code")]
            public XmlNode[] CDataErrorCode
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ErrorCode) };
                }
                set
                {
                    if (value == null)
                    {
                        ErrorCode = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ErrorCode = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ErrorCode = null;
                        return;
                    }
                    ErrorCode = Content.Value;
                }
            }

            #endregion

            #region "错误代码描述"

            /// <summary>
            /// 错误代码描述
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ErrorCodeDescription { get; set; }

            /// <summary>
            ///错误代码描述的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("err_code_des")]
            public XmlNode[] CDataErrorCodeDescription
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ErrorCodeDescription) };
                }
                set
                {
                    if (value == null)
                    {
                        ErrorCodeDescription = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ErrorCodeDescription = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ErrorCodeDescription = null;
                        return;
                    }
                    ErrorCodeDescription = Content.Value;
                }
            }

            #endregion

            #region "用户标识"

            /// <summary>
            /// 用户标识
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String OpenID { get; set; }

            /// <summary>
            /// 用户标识描述的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("openid")]
            public XmlNode[] CDataOpenID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(OpenID) };
                }
                set
                {
                    if (value == null)
                    {
                        OpenID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        OpenID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        OpenID = null;
                        return;
                    }
                    OpenID = Content.Value;
                }
            }

            #endregion

            #region "是否关注公众账号"

            /// <summary>
            /// 是否关注公众账号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String IsSubscribe { get; set; }

            /// <summary>
            /// 是否关注公众账号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("is_subscribe")]
            public XmlNode[] CDataIsSubscribe
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(IsSubscribe) };
                }
                set
                {
                    if (value == null)
                    {
                        IsSubscribe = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        IsSubscribe = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        IsSubscribe = null;
                        return;
                    }
                    IsSubscribe = Content.Value;
                }
            }

            #endregion

            #region "交易类型"

            /// <summary>
            /// 交易类型
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String TradeType { get; set; }

            /// <summary>
            /// 交易类型的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("trade_type")]
            public XmlNode[] CDataTradeType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(TradeType) };
                }
                set
                {
                    if (value == null)
                    {
                        TradeType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        TradeType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        TradeType = null;
                        return;
                    }
                    TradeType = Content.Value;
                }
            }

            #endregion

            #region "付款银行"

            /// <summary>
            /// 付款银行
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String BankType { get; set; }

            /// <summary>
            /// 付款银行的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("bank_type")]
            public XmlNode[] CDataBankType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(BankType) };
                }
                set
                {
                    if (value == null)
                    {
                        BankType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        BankType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        BankType = null;
                        return;
                    }
                    BankType = Content.Value;
                }
            }

            #endregion

            #region "总金额"

            /// <summary>
            /// 总金额
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String CashFee { get; set; }

            /// <summary>
            /// 总金额的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("cash_fee")]
            public XmlNode[] CDataCashFee
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(CashFee) };
                }
                set
                {
                    if (value == null)
                    {
                        CashFee = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        CashFee = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        CashFee = null;
                        return;
                    }
                    CashFee = Content.Value;
                }
            }

            #endregion

            /// <summary>
            /// 总金额
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("total_fee")]
            public Int32 TotalFee { get; set; }

            /// <summary>
            /// 现金券金额
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("coupon_fee")]
            [DataMember(Name = "coupon_fee", IsRequired = false)]
            public Int32? CouponFee { get; set; }

            #region "货币种类"

            /// <summary>
            /// 货币种类
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String FeeType { get; set; }

            /// <summary>
            /// 货币种类的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("fee_type")]
            public XmlNode[] CDataFeeType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(FeeType) };
                }
                set
                {
                    if (value == null)
                    {
                        FeeType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        FeeType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        FeeType = null;
                        return;
                    }
                    FeeType = Content.Value;
                }
            }

            #endregion

            #region "微信支付订单号"

            /// <summary>
            /// 微信支付订单号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String TransactionID { get; set; }

            /// <summary>
            /// 微信支付订单号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("transaction_id")]
            public XmlNode[] CDataTransactionID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(TransactionID) };
                }
                set
                {
                    if (value == null)
                    {
                        TransactionID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        TransactionID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        TransactionID = null;
                        return;
                    }
                    TransactionID = Content.Value;
                }
            }

            #endregion

            #region "商户订单号"

            /// <summary>
            /// 商户订单号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String OutTradeID { get; set; }

            /// <summary>
            /// 商户订单号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("out_trade_no")]
            public XmlNode[] CDataOutTradeID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(OutTradeID) };
                }
                set
                {
                    if (value == null)
                    {
                        OutTradeID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        OutTradeID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        OutTradeID = null;
                        return;
                    }
                    OutTradeID = Content.Value;
                }
            }

            #endregion

            #region "商家数据包"

            /// <summary>
            /// 商家数据包
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Attach { get; set; }

            /// <summary>
            /// 商家数据包的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("attach")]
            public XmlNode[] CDataAttach
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Attach) };
                }
                set
                {
                    if (value == null)
                    {
                        Attach = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Attach = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Attach = null;
                        return;
                    }
                    Attach = Content.Value;
                }
            }

            #endregion

            #region "支付完成时间"

            /// <summary>
            /// 支付完成时间
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String TimeEnd { get; set; }

            /// <summary>
            /// 支付完成时间的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("time_end")]
            public XmlNode[] CDataTimeEnd
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(TimeEnd) };
                }
                set
                {
                    if (value == null)
                    {
                        TimeEnd = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        TimeEnd = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        TimeEnd = null;
                        return;
                    }
                    TimeEnd = Content.Value;
                }
            }

            #endregion
        }

        [XmlRoot("xml")]
        [DataContract()]
        /// <summary>
        /// 微信支付通知返回
        /// </summary>
        public class JTTWechatWXPayNotifyReturn
        {

            #region "返回状态码"

            /// <summary>
            /// 返回状态码
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ReturnCode { get; set; }

            /// <summary>
            /// 返回状态码的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("return_code")]
            [DataMember(Name = "return_code", IsRequired = true)]
            public XmlNode[] CDataReturnCode
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ReturnCode) };
                }
                set
                {
                    if (value == null)
                    {
                        ReturnCode = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ReturnCode = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ReturnCode = null;
                        return;
                    }
                    ReturnCode = Content.Value;
                }
            }

            #endregion

            #region "返回信息"

            /// <summary>
            /// 返回信息
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ReturnMessage { get; set; }

            /// <summary>
            /// 返回信息的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("return_msg")]
            [DataMember(Name = "return_msg", IsRequired = false)]
            public XmlNode[] CDataReturnMessage
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ReturnMessage) };
                }
                set
                {
                    if (value == null)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    ReturnMessage = Content.Value;
                }
            }

            #endregion
        }

        /// <summary>
        /// 微信支付签名信息
        /// </summary>
        public class JTTWechatWXPaySignInfo
        {
            /// <summary>
            /// APPID
            /// </summary>
            public String AppID { get; set; }

            /// <summary>
            /// 参数包
            /// </summary>
            public String Package { get; set; }

            /// <summary>
            /// 临时串
            /// </summary>
            public String NonceString { get; set; }

            /// <summary>
            /// 时间戳
            /// </summary>
            [DataMember(Name = "TimeStamp")]
            public String TimeStamp { get; set; }

            /// <summary>
            /// 支付的Key
            /// </summary>
            public String WechatPayAPI { get; set; }
        }

        /// <summary>
        /// 微信支付签名返回
        /// </summary>
        public class JTTWechatWXPaySignReturn
        {
            /// <summary>
            /// 支付签名
            /// </summary>
            public String PaySign { get; set; }
        }


        #region  "微信统一下单"
        /// <summary>
        /// 微信统一下单信息
        /// </summary>
        [XmlRoot("xml")]
        [DataContract()]
        public class JTTWechatWXPayUnifiedOrder
        {
            /// <summary>
            /// 公众账号ID
            /// </summary>
            [XmlElement("appid")]
            [DataMember(Name = "appid", IsRequired = true)]
            public String APPID { get; set; }

            /// <summary>
            /// 商户号
            /// </summary>
            [XmlElement("mch_id")]
            [DataMember(Name = "mch_id", IsRequired = true)]
            public String MerchantID { get; set; }

            /// <summary>
            /// 设备号（可为空）
            /// </summary>
            [XmlElement("device_info")]
            [DataMember(Name = "device_info", IsRequired = false)]
            public String DeviceInfo { get; set; }

            /// <summary>
            /// 随机字符串
            /// </summary>
            [XmlElement("nonce_str")]
            [DataMember(Name = "nonce_str", IsRequired = true)]
            public String NonceString { get; set; }

            /// <summary>
            /// 签名
            /// </summary>
            [XmlElement("sign")]
            [DataMember(Name = "sign", IsRequired = true)]
            public String Sign { get; set; }

            /// <summary>
            /// 商品描述
            /// </summary>
            [XmlElement("body")]
            [DataMember(Name = "body", IsRequired = true)]
            public String Body { get; set; }

            /// <summary>
            /// 商品详情
            /// </summary>
            [XmlElement("detail")]
            [DataMember(Name = "detail", IsRequired = true)]
            public String Detail { get; set; }

            /// <summary>
            /// 附加数据
            /// </summary>
            [XmlElement("attach")]
            [DataMember(Name = "attach", IsRequired = false)]
            public String Attach { get; set; }

            /// <summary>
            /// 商户订单号
            /// </summary>
            [XmlElement("out_trade_no")]
            [DataMember(Name = "out_trade_no", IsRequired = true)]
            public String OutTradeID { get; set; }

            /// <summary>
            /// 货币类型
            /// </summary>
            [XmlElement("fee_type")]
            [DataMember(Name = "fee_type", IsRequired = false)]
            public String FeeType { get; set; }

            /// <summary>
            /// 总金额
            /// </summary>
            [XmlElement("total_fee")]
            [DataMember(Name = "total_fee", IsRequired = true)]
            public Int32 TotalFee { get; set; }

            /// <summary>
            /// 终端IP
            /// </summary>
            [XmlElement("spbill_create_ip")]
            [DataMember(Name = "spbill_create_ip", IsRequired = true)]
            public String SpbillCreateIP { get; set; }

            /// <summary>
            /// 交易起始时间
            /// </summary>
            [XmlElement("time_start")]
            [DataMember(Name = "time_start", IsRequired = false)]
            public String TimeStart { get; set; }

            /// <summary>
            /// 交易结束时间
            /// </summary>
            [XmlElement("time_expire")]
            [DataMember(Name = "time_expire", IsRequired = false)]
            public String TimeExpire { get; set; }

            /// <summary>
            /// 商品标记
            /// </summary>
            [XmlElement("goods_tag")]
            [DataMember(Name = "goods_tag", IsRequired = false)]
            public String GoodsTag { get; set; }

            /// <summary>
            /// 通知地址
            /// </summary>
            [XmlElement("notify_url")]
            [DataMember(Name = "notify_url", IsRequired = true)]
            public String NotifyURL { get; set; }

            /// <summary>
            /// 交易类型
            /// </summary>
            [XmlElement("trade_type")]
            [DataMember(Name = "trade_type", IsRequired = true)]
            public String TradeType { get; set; }

            /// <summary>
            /// 商品ID
            /// </summary>
            [XmlElement("product_id")]
            [DataMember(Name = "product_id", IsRequired = false)]
            public String ProductID { get; set; }

            /// <summary>
            /// 用户标识
            /// </summary>
            [XmlElement("openid")]
            [DataMember(Name = "openid", IsRequired = false)]
            public String OpenID { get; set; }

        }

        /// <summary>
        /// 微信统一下单信息返回
        /// </summary>
        [XmlRoot("xml")]
        public class JTTWechatWXPayUnifiedOrderReturn
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatWXPayUnifiedOrderReturn() { }

            #region "返回状态码"

            /// <summary>
            /// 返回状态码
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ReturnCode { get; set; }

            /// <summary>
            /// 返回状态码的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("return_code")]
            public XmlNode[] CDataReturnCode
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ReturnCode) };
                }
                set
                {
                    if (value == null)
                    {
                        ReturnCode = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ReturnCode = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ReturnCode = null;
                        return;
                    }
                    ReturnCode = Content.Value;
                }
            }

            #endregion

            #region "返回信息"

            /// <summary>
            /// 返回信息
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ReturnMessage { get; set; }

            /// <summary>
            /// 返回信息的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("return_msg")]
            public XmlNode[] CDataReturnMessage
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ReturnMessage) };
                }
                set
                {
                    if (value == null)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    ReturnMessage = Content.Value;
                }
            }

            #endregion

            #region "公众账号ID"

            /// <summary>
            /// 公众账号ID
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String AppID { get; set; }

            /// <summary>
            /// 公众账号ID的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("appid")]
            public XmlNode[] CDataAppID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(AppID) };
                }
                set
                {
                    if (value == null)
                    {
                        AppID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        AppID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        AppID = null;
                        return;
                    }
                    AppID = Content.Value;
                }
            }

            #endregion

            #region "商户号"

            /// <summary>
            /// 商户号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String MerchantID { get; set; }

            /// <summary>
            /// 商户号ID的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("mch_id")]
            public XmlNode[] CDataMerchantID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(MerchantID) };
                }
                set
                {
                    if (value == null)
                    {
                        MerchantID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        MerchantID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        MerchantID = null;
                        return;
                    }
                    MerchantID = Content.Value;
                }
            }

            #endregion

            #region "设备号"

            /// <summary>
            /// 设备号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String DeviceInfo { get; set; }

            /// <summary>
            /// 设备号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("device_info")]
            public XmlNode[] CDataDeviceInfo
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(DeviceInfo) };
                }
                set
                {
                    if (value == null)
                    {
                        DeviceInfo = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        DeviceInfo = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        DeviceInfo = null;
                        return;
                    }
                    DeviceInfo = Content.Value;
                }
            }

            #endregion

            #region "随机字符串"

            /// <summary>
            /// 随机字符串
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String NonceString { get; set; }

            /// <summary>
            /// 随机字符串的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("nonce_str")]
            public XmlNode[] CDataNonceString
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(NonceString) };
                }
                set
                {
                    if (value == null)
                    {
                        NonceString = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        NonceString = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        NonceString = null;
                        return;
                    }
                    NonceString = Content.Value;
                }
            }

            #endregion

            #region "签名"

            /// <summary>
            /// 签名
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Sign { get; set; }

            /// <summary>
            /// 签名的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("sign")]
            public XmlNode[] CDataSign
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Sign) };
                }
                set
                {
                    if (value == null)
                    {
                        Sign = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Sign = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Sign = null;
                        return;
                    }
                    Sign = Content.Value;
                }
            }

            #endregion

            #region "业务结果"

            /// <summary>
            /// 业务结果
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ResultCode { get; set; }

            /// <summary>
            /// 业务结果的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("result_code")]
            public XmlNode[] CDataResultCode
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ResultCode) };
                }
                set
                {
                    if (value == null)
                    {
                        ResultCode = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ResultCode = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ResultCode = null;
                        return;
                    }
                    ResultCode = Content.Value;
                }
            }

            #endregion

            #region "错误代码"

            /// <summary>
            /// 错误代码
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ErrorCode { get; set; }

            /// <summary>
            /// 错误代码的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("err_code")]
            public XmlNode[] CDataErrorCode
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ErrorCode) };
                }
                set
                {
                    if (value == null)
                    {
                        ErrorCode = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ErrorCode = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ErrorCode = null;
                        return;
                    }
                    ErrorCode = Content.Value;
                }
            }

            #endregion

            #region "错误代码描述"

            /// <summary>
            /// 错误代码描述
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ErrorCodeDes { get; set; }

            /// <summary>
            /// 错误代码描述的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("err_code_des")]
            public XmlNode[] CDataErrorCodeDes
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ErrorCodeDes) };
                }
                set
                {
                    if (value == null)
                    {
                        ErrorCodeDes = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ErrorCodeDes = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ErrorCodeDes = null;
                        return;
                    }
                    ErrorCodeDes = Content.Value;
                }
            }

            #endregion

            #region "交易类型"

            /// <summary>
            /// 交易类型
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String TradeType { get; set; }

            /// <summary>
            /// 交易类型的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("trade_type")]
            public XmlNode[] CDataTradeType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(TradeType) };
                }
                set
                {
                    if (value == null)
                    {
                        TradeType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        TradeType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        TradeType = null;
                        return;
                    }
                    TradeType = Content.Value;
                }
            }

            #endregion

            #region "预支付交易会话标识"

            /// <summary>
            /// 预支付交易会话标识
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String PrepayID { get; set; }

            /// <summary>
            /// 预支付交易会话标识的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("prepay_id")]
            public XmlNode[] CDataPrepayID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(PrepayID) };
                }
                set
                {
                    if (value == null)
                    {
                        PrepayID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        PrepayID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        PrepayID = null;
                        return;
                    }
                    PrepayID = Content.Value;
                }
            }

            #endregion

            #region "二维码链接"

            /// <summary>
            /// 二维码链接
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String CodeURL { get; set; }

            /// <summary>
            /// 二维码链接的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("code_url")]
            public XmlNode[] CDataCodeURL
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(CodeURL) };
                }
                set
                {
                    if (value == null)
                    {
                        CodeURL = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        CodeURL = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        CodeURL = null;
                        return;
                    }
                    CodeURL = Content.Value;
                }
            }

            #endregion
        }

        #endregion

        #region "订单查询"


        /// <summary>
        /// 订单查询请求信息
        /// </summary>
        [XmlRoot("xml")]
        [DataContract()]
        public class JTTWechatWXPayOrderQueryInfo
        {
            /// <summary>
            /// 公众账号ID
            /// </summary>
            [XmlElement("appid")]
            [DataMember(Name = "appid", IsRequired = true)]
            public String APPID { get; set; }

            /// <summary>
            /// 商户号
            /// </summary>
            [XmlElement("mch_id")]
            [DataMember(Name = "mch_id", IsRequired = true)]
            public String MerchantID { get; set; }

            /// <summary>
            /// 微信订单号
            /// </summary>
            [XmlElement("transaction_id")]
            [DataMember(Name = "transaction_id", IsRequired = false)]
            public String TransactionID { get; set; }

            /// <summary>
            /// 商户订单号
            /// </summary>
            [XmlElement("out_trade_no")]
            [DataMember(Name = "out_trade_no", IsRequired = false)]
            public String OutTradeID { get; set; }

            /// <summary>
            /// 随机字符串
            /// </summary>
            [XmlElement("nonce_str")]
            [DataMember(Name = "nonce_str", IsRequired = true)]
            public String NonceString { get; set; }

            /// <summary>
            /// 签名
            /// </summary>
            [XmlElement("sign")]
            [DataMember(Name = "sign", IsRequired = true)]
            public String Sign { get; set; }
        }

        /// <summary>
        /// 订单查询请求响应
        /// </summary>
        [XmlRoot("xml")]
        public class JTTWechatWXPayOrderQueryReturn
        {
            #region "返回状态码"

            /// <summary>
            /// 返回状态码
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ReturnCode { get; set; }

            /// <summary>
            /// 返回状态码的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("return_code")]
            public XmlNode[] CDataReturnCode
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ReturnCode) };
                }
                set
                {
                    if (value == null)
                    {
                        ReturnCode = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ReturnCode = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ReturnCode = null;
                        return;
                    }
                    ReturnCode = Content.Value;
                }
            }

            #endregion

            #region "返回信息"

            /// <summary>
            /// 返回信息
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ReturnMessage { get; set; }

            /// <summary>
            /// 返回信息的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("return_msg")]
            public XmlNode[] CDataReturnMessage
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ReturnMessage) };
                }
                set
                {
                    if (value == null)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ReturnMessage = null;
                        return;
                    }
                    ReturnMessage = Content.Value;
                }
            }

            #endregion

            #region "公众账号ID"

            /// <summary>
            /// 公众账号ID
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String AppID { get; set; }

            /// <summary>
            /// 公众账号ID的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("appid")]
            public XmlNode[] CDataAppID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(AppID) };
                }
                set
                {
                    if (value == null)
                    {
                        AppID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        AppID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        AppID = null;
                        return;
                    }
                    AppID = Content.Value;
                }
            }

            #endregion

            #region "商户号"

            /// <summary>
            /// 商户号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String MerchantID { get; set; }

            /// <summary>
            /// 商户号ID的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("mch_id")]
            public XmlNode[] CDataMerchantID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(MerchantID) };
                }
                set
                {
                    if (value == null)
                    {
                        MerchantID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        MerchantID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        MerchantID = null;
                        return;
                    }
                    MerchantID = Content.Value;
                }
            }

            #endregion

            #region "随机字符串"

            /// <summary>
            /// 随机字符串
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String NonceString { get; set; }

            /// <summary>
            /// 随机字符串的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("nonce_str")]
            public XmlNode[] CDataNonceString
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(NonceString) };
                }
                set
                {
                    if (value == null)
                    {
                        NonceString = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        NonceString = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        NonceString = null;
                        return;
                    }
                    NonceString = Content.Value;
                }
            }

            #endregion

            #region "签名"

            /// <summary>
            /// 签名
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Sign { get; set; }

            /// <summary>
            /// 签名的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("sign")]
            public XmlNode[] CDataSign
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Sign) };
                }
                set
                {
                    if (value == null)
                    {
                        Sign = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Sign = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Sign = null;
                        return;
                    }
                    Sign = Content.Value;
                }
            }

            #endregion

            #region "业务结果"

            /// <summary>
            /// 业务结果
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ResultCode { get; set; }

            /// <summary>
            /// 业务结果的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("result_code")]
            public XmlNode[] CDataResultCode
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ResultCode) };
                }
                set
                {
                    if (value == null)
                    {
                        ResultCode = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ResultCode = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ResultCode = null;
                        return;
                    }
                    ResultCode = Content.Value;
                }
            }

            #endregion

            #region "错误代码"

            /// <summary>
            /// 错误代码
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ErrorCode { get; set; }

            /// <summary>
            /// 错误代码的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("err_code")]
            public XmlNode[] CDataErrorCode
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ErrorCode) };
                }
                set
                {
                    if (value == null)
                    {
                        ErrorCode = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ErrorCode = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ErrorCode = null;
                        return;
                    }
                    ErrorCode = Content.Value;
                }
            }

            #endregion

            #region "错误代码描述"

            /// <summary>
            /// 错误代码描述
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String ErrorCodeDes { get; set; }

            /// <summary>
            /// 错误代码描述的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("err_code_des")]
            public XmlNode[] CDataErrorCodeDes
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(ErrorCodeDes) };
                }
                set
                {
                    if (value == null)
                    {
                        ErrorCodeDes = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        ErrorCodeDes = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        ErrorCodeDes = null;
                        return;
                    }
                    ErrorCodeDes = Content.Value;
                }
            }

            #endregion

            #region "设备号"

            /// <summary>
            /// 设备号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String DeviceInfo { get; set; }

            /// <summary>
            /// 设备号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("device_info")]
            public XmlNode[] CDataDeviceInfo
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(DeviceInfo) };
                }
                set
                {
                    if (value == null)
                    {
                        DeviceInfo = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        DeviceInfo = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        DeviceInfo = null;
                        return;
                    }
                    DeviceInfo = Content.Value;
                }
            }

            #endregion

            #region "用户标识"

            /// <summary>
            /// 用户标识
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String OpenID { get; set; }

            /// <summary>
            /// 用户标识的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("openid")]
            public XmlNode[] CDataOpenID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(OpenID) };
                }
                set
                {
                    if (value == null)
                    {
                        OpenID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        OpenID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        OpenID = null;
                        return;
                    }
                    OpenID = Content.Value;
                }
            }

            #endregion

            #region "是否关注公众账号"

            /// <summary>
            /// 是否关注公众账号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String IsSubscribe { get; set; }

            /// <summary>
            /// 是否关注公众账号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("is_subscribe")]
            public XmlNode[] CDataIsSubscribe
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(IsSubscribe) };
                }
                set
                {
                    if (value == null)
                    {
                        IsSubscribe = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        IsSubscribe = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        IsSubscribe = null;
                        return;
                    }
                    IsSubscribe = Content.Value;
                }
            }

            #endregion

            #region "交易类型"

            /// <summary>
            /// 交易类型
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String TradeType { get; set; }

            /// <summary>
            /// 交易类型的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("trade_type")]
            public XmlNode[] CDataTradeType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(TradeType) };
                }
                set
                {
                    if (value == null)
                    {
                        TradeType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        TradeType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        TradeType = null;
                        return;
                    }
                    TradeType = Content.Value;
                }
            }

            #endregion

            #region "交易状态"

            /// <summary>
            /// 交易状态
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String TradeState { get; set; }

            /// <summary>
            /// 交易状态的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("trade_state")]
            public XmlNode[] CDataTradeState
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(TradeState) };
                }
                set
                {
                    if (value == null)
                    {
                        TradeState = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        TradeState = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        TradeState = null;
                        return;
                    }
                    TradeState = Content.Value;
                }
            }

            #endregion

            #region "付款银行"

            /// <summary>
            /// 付款银行
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String BankType { get; set; }

            /// <summary>
            /// 付款银行的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("bank_type")]
            public XmlNode[] CDataBankType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(BankType) };
                }
                set
                {
                    if (value == null)
                    {
                        BankType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        BankType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        BankType = null;
                        return;
                    }
                    BankType = Content.Value;
                }
            }

            #endregion

            #region "总金额"

            /// <summary>
            /// 总金额
            /// </summary>
            [XmlIgnore]
            public Int32 TotalFee { get; set; }

            /// <summary>
            /// 总金额
            /// </summary>
            [XmlElement("total_fee")]
            public String TotalFeeAsText
            {
                get
                {
                    return TotalFee.ToString();
                }
                set
                {
                    TotalFee = Convert.ToInt32(value);
                }
            }

            #endregion

            #region "货币种类"

            /// <summary>
            /// 货币种类
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String FeeType { get; set; }

            /// <summary>
            /// 货币种类的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("fee_type")]
            public XmlNode[] CDataFeeType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(FeeType) };
                }
                set
                {
                    if (value == null)
                    {
                        FeeType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        FeeType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        FeeType = null;
                        return;
                    }
                    FeeType = Content.Value;
                }
            }

            #endregion

            #region "现金支付金额"

            /// <summary>
            /// 现金支付金额
            /// </summary>
            [XmlIgnore]
            public Int32 CashFee { get; set; }

            /// <summary>
            /// 现金支付金额
            /// </summary>
            [XmlElement("cash_fee")]
            public String CashFeeAsText
            {
                get
                {
                    return CashFee.ToString();
                }
                set
                {
                    CashFee = Convert.ToInt32(value);
                }
            }

            #endregion

            #region "现金支付货币类型"

            /// <summary>
            /// 现金支付货币类型
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String CashFeeType { get; set; }

            /// <summary>
            /// 现金支付货币类型的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("cash_fee_type")]
            public XmlNode[] CDataCashFeeType
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(CashFeeType) };
                }
                set
                {
                    if (value == null)
                    {
                        CashFeeType = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        CashFeeType = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        CashFeeType = null;
                        return;
                    }
                    CashFeeType = Content.Value;
                }
            }

            #endregion

            #region "现金券金额"

            /// <summary>
            /// 现金券金额
            /// </summary>
            [XmlIgnore]
            public Int32? CouponFee { get; set; }

            /// <summary>
            /// 现金券金额
            /// </summary>
            [XmlElement("coupon_fee")]
            public String CouponFeeAsText
            {
                get
                {
                    return (CouponFee.HasValue) ? CouponFee.ToString() : null;
                }
                set
                {
                    CouponFee = !String.IsNullOrEmpty(value) ? Int32.Parse(value) : default(Int32?);
                }
            }

            #endregion

            #region "代金券或立减优惠使用数量"

            /// <summary>
            /// 代金券或立减优惠使用数量
            /// </summary>
            [XmlIgnore]
            public Int32? CouponCount { get; set; }

            /// <summary>
            /// 代金券或立减优惠使用数量
            /// </summary>
            [XmlElement("coupon_count")]
            public String CouponCountAsText
            {
                get
                {
                    return (CouponCount.HasValue) ? CouponCount.ToString() : null;
                }
                set
                {
                    CouponCount = !String.IsNullOrEmpty(value) ? Int32.Parse(value) : default(Int32?);
                }
            }

            #endregion

            #region "微信支付订单号"

            /// <summary>
            /// 微信支付订单号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String TransactionID { get; set; }

            /// <summary>
            /// 微信支付订单号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("transaction_id")]
            public XmlNode[] CDataTransactionID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(TransactionID) };
                }
                set
                {
                    if (value == null)
                    {
                        TransactionID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        TransactionID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        TransactionID = null;
                        return;
                    }
                    TransactionID = Content.Value;
                }
            }

            #endregion

            #region "商户订单号"

            /// <summary>
            /// 商户订单号
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String OutTradeID { get; set; }

            /// <summary>
            /// 商户订单号的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("out_trade_no")]
            public XmlNode[] CDataOutTradeID
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(OutTradeID) };
                }
                set
                {
                    if (value == null)
                    {
                        OutTradeID = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        OutTradeID = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        OutTradeID = null;
                        return;
                    }
                    OutTradeID = Content.Value;
                }
            }

            #endregion

            #region "商家数据包"

            /// <summary>
            /// 商家数据包
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String Attach { get; set; }

            /// <summary>
            /// 商家数据包的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("attach")]
            public XmlNode[] CDataAttach
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(Attach) };
                }
                set
                {
                    if (value == null)
                    {
                        Attach = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        Attach = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        Attach = null;
                        return;
                    }
                    Attach = Content.Value;
                }
            }

            #endregion

            #region "支付完成时间"

            /// <summary>
            /// 支付完成时间
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String TimeEnd { get; set; }

            /// <summary>
            /// 支付完成时间的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("time_end")]
            public XmlNode[] CDataTimeEnd
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(TimeEnd) };
                }
                set
                {
                    if (value == null)
                    {
                        TimeEnd = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        TimeEnd = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        TimeEnd = null;
                        return;
                    }
                    TimeEnd = Content.Value;
                }
            }

            #endregion

            #region "交易状态描述"

            /// <summary>
            /// 交易状态描述
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlIgnore()]
            public String TradeStateDescription { get; set; }

            /// <summary>
            /// 交易状态描述的CDATA处理属性
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            [XmlElement("trade_state_desc")]
            public XmlNode[] CDataTradeStateDescription
            {
                get
                {
                    XmlDocument MyXmlDocument = new XmlDocument();
                    return new XmlNode[] { MyXmlDocument.CreateCDataSection(TradeStateDescription) };
                }
                set
                {
                    if (value == null)
                    {
                        TradeStateDescription = null;
                        return;
                    }
                    if (value.Length != 1)
                    {
                        TradeStateDescription = null;
                        return;
                    }
                    dynamic Content = value[0];
                    if (Content == null)
                    {
                        TradeStateDescription = null;
                        return;
                    }
                    TradeStateDescription = Content.Value;
                }
            }

            #endregion
        }

        #endregion

        #endregion

        #region 共享收货地址

        /// <summary>
        /// 地址签名信息
        /// </summary>
        public class JTTWechatAddressSignInfo
        {
            /// <summary>
            /// APPID
            /// </summary>
            public String AppID { get; set; }

            /// <summary>
            /// 网页地址
            /// </summary>
            public String URL { get; set; }

            /// <summary>
            /// 时间戳
            /// </summary>
            public String TimeStamp { get; set; }

            /// <summary>
            /// 随机字符串
            /// </summary>
            public String NonceString { get; set; }

            /// <summary>
            /// 授权码
            /// </summary>
            public String AccessToken { get; set; }
        }

        /// <summary>
        /// 地址签名返回
        /// </summary>
        public class JTTWechatAddressSignReturn
        {
            /// <summary>
            /// 签名信息
            /// </summary>
            public String Sign { get; set; }
        }

        #endregion

        #region 带参数的二维码

        #region 永久二维码

        /// <summary>
        /// 永久参数的二维码
        /// </summary>
        [DataContract()]
        public class JTTWechatQRLimitScene
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatQRLimitScene(String _SceneString)
            {
                ActionName = "QR_LIMIT_STR_SCENE";//永久
                ActionInfo = new JTTWechatQRLimitSceneActionInfo(_SceneString);
            }

            /// <summary>
            /// 动作名称
            /// </summary>
            [DataMember(Name = "action_name")]
            public String ActionName { get; set; }

            /// <summary>
            /// 动作信息
            /// </summary>
            [DataMember(Name = "action_info")]
            public JTTWechatQRLimitSceneActionInfo ActionInfo { get; set; }

        }


        /// <summary>
        /// 永久参数的二维码动作信息
        /// </summary>
        [DataContract()]
        public class JTTWechatQRLimitSceneActionInfo
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatQRLimitSceneActionInfo(String _SceneString)
            {
                Scene = new JTTWechatQRLimitSceneActionInfoSceneString(_SceneString);
            }

            /// <summary>
            /// 场景
            /// </summary>
            [DataMember(Name = "scene")]
            public JTTWechatQRLimitSceneActionInfoSceneString Scene { get; set; }
        }

        /// <summary>
        /// 永久参数的二维码动作信息场景值
        /// </summary>
        [DataContract()]
        public class JTTWechatQRLimitSceneActionInfoSceneString
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            /// <param name="_SceneString"></param>
            public JTTWechatQRLimitSceneActionInfoSceneString(String _SceneString)
            {
                SceneString = _SceneString;
            }

            /// <summary>
            /// 场景值
            /// </summary>
            [DataMember(Name = "scene_str")]
            public String SceneString { get; set; }
        }

        /// <summary>
        /// 生成永久二维码返回
        /// </summary>
        [DataContract()]
        public class JTTWechatQRLimitSceneReturn
        {
            /// <summary>
            /// 钥匙
            /// </summary>
            [DataMember(Name = "ticket")]
            public String Ticket { get; set; }

            /// <summary>
            /// 二维码真实地址文字
            /// </summary>
            [DataMember(Name = "url")]
            public String URL { get; set; }

            /// <summary>
            /// 过期时间
            /// </summary>
            [DataMember(Name = "expire_seconds")]
            public Int32 ExpireSeconds { get; set; }
        }

        /// <summary>
        /// 永久二维码信息
        /// </summary>
        [DataContract()]
        public class JTTWechatQRLimitSceneInfo
        {
            /// <summary>
            /// 图像文本
            /// </summary>
            [DataMember(Name = "ImageText")]
            public String ImageText { get; set; }

            /// <summary>
            /// 图像地址
            /// </summary>
            [DataMember(Name = "ImageURL")]
            public String ImageURL { get; set; }
        }

        #endregion

        #region 临时二维码

        /// <summary>
        /// 临时参数的二维码
        /// </summary>
        [DataContract()]
        public class JTTWechatQRTempScene
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatQRTempScene(Int32 _SceneID, Int32 _ExpireSeconds)
            {
                ActionName = "QR_SCENE";//临时
                ExpireSeconds = _ExpireSeconds;
                ActionInfo = new JTTWechatQRTempSceneActionInfo(_SceneID);
            }

            /// <summary>
            /// 动作名称
            /// </summary>
            [DataMember(Name = "action_name")]
            public String ActionName { get; set; }

            /// <summary>
            /// 过期秒数
            /// </summary>
            [DataMember(Name = "expire_seconds")]
            public Int32 ExpireSeconds { get; set; }

            /// <summary>
            /// 动作信息
            /// </summary>
            [DataMember(Name = "action_info")]
            public JTTWechatQRTempSceneActionInfo ActionInfo { get; set; }

        }


        /// <summary>
        /// 临时参数的二维码动作信息
        /// </summary>
        [DataContract()]
        public class JTTWechatQRTempSceneActionInfo
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatQRTempSceneActionInfo(Int32 _SceneID)
            {
                Scene = new JTTWechatQRTempSceneActionInfoSceneID(_SceneID);
            }

            /// <summary>
            /// 场景
            /// </summary>
            [DataMember(Name = "scene")]
            public JTTWechatQRTempSceneActionInfoSceneID Scene { get; set; }
        }

        /// <summary>
        /// 临时参数的二维码动作信息场景值
        /// </summary>
        [DataContract()]
        public class JTTWechatQRTempSceneActionInfoSceneID
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            /// <param name="_SceneString"></param>
            public JTTWechatQRTempSceneActionInfoSceneID(Int32 _SceneID)
            {
                SceneID = _SceneID;
            }

            /// <summary>
            /// 场景值
            /// </summary>
            [DataMember(Name = "scene_id")]
            public Int32 SceneID { get; set; }
        }

        /// <summary>
        /// 生成永久二维码返回
        /// </summary>
        [DataContract()]
        public class JTTWechatQRTempSceneReturn
        {
            /// <summary>
            /// 钥匙
            /// </summary>
            [DataMember(Name = "ticket")]
            public String Ticket { get; set; }

            /// <summary>
            /// 二维码真实地址文字
            /// </summary>
            [DataMember(Name = "url")]
            public String URL { get; set; }

            /// <summary>
            /// 过期时间
            /// </summary>
            [DataMember(Name = "expire_seconds")]
            public Int32 ExpireSeconds { get; set; }
        }

        /// <summary>
        /// 临时二维码信息
        /// </summary>
        [DataContract()]
        public class JTTWechatQRTempSceneInfo
        {
            /// <summary>
            /// 图像文本
            /// </summary>
            [DataMember(Name = "ImageText")]
            public String ImageText { get; set; }

            /// <summary>
            /// 图像地址
            /// </summary>
            [DataMember(Name = "ImageURL")]
            public String ImageURL { get; set; }
        }

        #endregion

        #endregion

        #region 客服消息

        /// <summary>
        /// 客服文本消息
        /// </summary>
        [DataContract()]
        public class JTTWechatCustomTextMessage
        {

            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatCustomTextMessage()
            {
                MessageType = "text";
                Text = new JTTWechatCustomTextMessageContent();
            }
            /// <summary>
            /// 接收者
            /// </summary>
            [DataMember(Name = "touser")]
            public String Touser { get; set; }

            /// <summary>
            /// 消息类型
            /// </summary>
            [DataMember(Name = "msgtype")]
            public String MessageType { get; set; }

            /// <summary>
            /// 文本内容
            /// </summary>
            [DataMember(Name = "text")]
            public JTTWechatCustomTextMessageContent Text { get; set; }
        }

        /// <summary>
        /// 客服文本消息内容
        /// </summary>
        [DataContract()]
        public class JTTWechatCustomTextMessageContent
        {
            /// <summary>
            /// 内容
            /// </summary>
            [DataMember(Name = "content")]
            public String Content { get; set; }
        }

        /// <summary>
        /// 客服消息返回结果
        /// </summary>
        [DataContract()]
        public class JTTWechatCustomMessageReturn
        {
            /// <summary>
            /// 错误代码
            /// </summary>
            [DataMember(Name = "errcode")]
            public Int32 ErrorCode { get; set; }

            /// <summary>
            /// 错误消息
            /// </summary>
            [DataMember(Name = "errmsg")]
            public String ErrorMessage { get; set; }
        }

        /// <summary>
        /// 客服图片消息
        /// </summary>
        [DataContract()]
        public class JTTWechatCustomImageMessage
        {

            /// <summary>
            /// 构造函数
            /// </summary>
            public JTTWechatCustomImageMessage()
            {
                MessageType = "image";
                Image = new JTTWechatCustomImageMessageContent();
            }
            /// <summary>
            /// 接收者
            /// </summary>
            [DataMember(Name = "touser")]
            public String Touser { get; set; }

            /// <summary>
            /// 消息类型
            /// </summary>
            [DataMember(Name = "msgtype")]
            public String MessageType { get; set; }

            /// <summary>
            /// 图片内容
            /// </summary>
            [DataMember(Name = "image")]
            public JTTWechatCustomImageMessageContent Image { get; set; }
        }

        /// <summary>
        /// 客服图片消息内容
        /// </summary>
        [DataContract()]
        public class JTTWechatCustomImageMessageContent
        {
            /// <summary>
            /// 素材编号
            /// </summary>
            [DataMember(Name = "media_id")]
            public String MediaID { get; set; }
        }



        #endregion

        #region 素材管理

        /// <summary>
        /// 媒体素材上传返回
        /// </summary>
        [DataContract()]
        public class JTTWechatMediaUploadReturn
        {
            /// <summary>
            /// 素材类型
            /// </summary>
            [DataMember(Name = "type")]
            public String Type { get; set; }

            [DataMember(Name = "media_id")]
            public String MediaID { get; set; }

            [DataMember(Name = "created_at")]
            public Int64 CreateTime { get; set; }
        }

        #endregion

        #region 多客服

        /// <summary>
        ///  多客服信息
        /// </summary>
        [DataContract()]
        public class JTTWechatKFInfo
        {
            /// <summary>
            /// 客服账号
            /// </summary>
            [DataMember(Name = "kf_account")]
            public String KFAccount { get; set; }

            /// <summary>
            /// 客服头像地址
            /// </summary>
            [DataMember(Name = "kf_headimgurl")]
            public String KFHeadURL { get; set; }

            /// <summary>
            /// 客服编号
            /// </summary>
            [DataMember(Name = "kf_id")]
            public String KFID { get; set; }

            /// <summary>
            /// 客服昵称
            /// </summary>
            [DataMember(Name = "kf_nick")]
            public String KFNickName { get; set; }
        }

        /// <summary>
        /// 获得客服列表返回
        /// </summary>
        [DataContract()]
        public class JTTWechatGetKFListReturn
        {
            /// <summary>
            /// 客服列表
            /// </summary>
            [DataMember(Name = "kf_list")]
            public List<JTTWechatKFInfo> KFList { get; set; }
        }

        /// <summary>
        /// 客服会话状态
        /// </summary>
        [DataContract()]
        public class JTTWechatKFSessionInfo
        {
            /// <summary>
            /// 创建时间
            /// </summary>
            [DataMember(Name = "createtime")]
            public Int64 CreateTime { get; set; }

            /// <summary>
            /// 错误代码
            /// </summary>
            [DataMember(Name = "errcode")]
            public Int32 ErrorCode { get; set; }

            /// <summary>
            /// 错误信息
            /// </summary>
            [DataMember(Name = "errmsg")]
            public String ErrorMessage { get; set; }

            /// <summary>
            /// 客服账号
            /// </summary>
            [DataMember(Name = "kf_account")]
            public String KFAccount { get; set; }
        }

        #endregion
    }
}
